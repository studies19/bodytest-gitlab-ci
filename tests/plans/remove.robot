***Settings***
Documentation           Remover planos

Resource                ${EXECDIR}/resources/base.robot

Suite Setup             Start Admin Session
Test Teardown           Take Screenshot


***Test Cases***
Cenario: Remover plano cadastrado

    ${plan}         Create Dictionary       title=Remove Test          duration=4           price=32.00         total=R$ 128,00

    
    Insert plan                  ${plan}
    Go To Plans
    Search Plan By Title         ${plan.title}
    Request Removal By Title     ${plan.title}
    Confirm Removal Plan
    Toaster Should Be            Plano removido com sucesso
    Student Should Not visible   ${plan.title}

    [Teardown]      Thinking and Take Screenshot  2 


Cenario: Desistir da exclusão

    ${plan}         Create Dictionary       title=Plano 8 Meses          duration=8           price=60.00         total=R$ 480,00

    
    Insert Plan                  ${plan}
    Go To Plans 
    Search Plan By Title         ${plan.title}
    Request Removal By Title     ${plan.title}
    Cancel Removal
    Plan Should Be visible       ${plan.title}

    Remove Plan                  ${plan.title}