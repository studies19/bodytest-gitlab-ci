***Settings***
Documentation           Cadastro de planos

Resource                ${EXECDIR}/resources/base.robot

Suite Setup             Start Admin Session
Test Teardown           Take Screenshot


***Test Cases***
Cenário: Calcular preço total do plano

    ${plan}         Create Dictionary       title=Papito Teste          duration=12           price=19,99         total=R$ 239,88

    Go To Plans
    Go to Form Plan
    Fill Plan Form               ${plan}
    Total Plan Should Be         ${plan.total}


Cenário: Novo plano
    
    ${plan}         Create Dictionary       title=Plano 5 Meses              duration=5           price=30,00             total=R$ 150,00

    Remove Plan            ${plan.title}
    Go To Plans
    Go to Form Plan
    Fill Plan Form         ${plan}
    Total Plan Should Be   ${plan.total}
    Submit Plan Form
    Toaster Should Be      Plano cadastrado com sucesso

    [Teardown]      Thinking and Take Screenshot  2

    Remove Plan            ${plan.title}         


Cenario: Campos título e duração devem ser obrigatórios

    @{expected_alerts}      Set Variable        Informe o título do plano      Informe a duração do plano em meses
    @{got_alerts}           Create List

    Go To Plans
    Go to Form Plan
    Submit Plan Form

    FOR     ${index}        IN RANGE      1     3
        ${span}             Get Required Alerts   ${index}
        Append To List      ${got_alerts}         ${span}
    END

    Log     ${expected_alerts}
    Log     ${got_alerts}

    Lists Should Be Equal       ${expected_alerts}      ${got_alerts}


Cenario: Validação dos campos numéricos

    [Template]  Check Type Field On Plan Form
    ${DURATION_FIELD}              number
