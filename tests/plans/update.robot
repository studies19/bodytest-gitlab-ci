***Settings***
Documentation           Atualizar planos

Resource                ${EXECDIR}/resources/base.robot

Suite Setup             Start Admin Session
Test Teardown           Take Screenshot


***Test Cases***
Cenário: Atualizar um plano já cadastrado

    ${fixture}          Get JSON            plans-update.json  

    ${plano10}         Set Variable        ${fixture['before']}
    ${plano11}         Set Variable        ${fixture['after']}

    Remove Plan          ${plano10['title']}
    Remove Plan          ${plano11['title']}

    Insert Plan                 ${plano10}
    Go To Plans
    Search Plan By Title        ${plano10['title']}
    Go To Plan Update Form      ${plano10['title']}
    Update A Plan               ${plano11}   
    Toaster Should Be           Plano Atualizado com sucesso

    [Teardown]      Thinking and Take Screenshot  2

    Remove Plan          ${plano10['title']}
    Remove Plan          ${plano11['title']}