***Settings***
Documentation           Buscar Planos

Resource                ${EXECDIR}/resources/base.robot

Suite Setup             Start Admin Session
Test Teardown           Take Screenshot


***Test Cases***
Cenario: Busca exata

    ${plan}         Create Dictionary       title=Plano 12 Meses          duration=12           price=19.99         total=R$ 239,88

    Remove Plan                     ${plan.title}
    Insert Plan                     ${plan}
    Go To Plans
    Search Plan By Title            ${plan.title}
    Plan Should Be visible          ${plan.title}
    Total Items Should Be             1

    Remove Plan                     ${plan.title}


Cenario: Registro não encontrado

    ${title}         Set Variable       Plano Vitalício

    Remove Plan                       ${title} 
    Go To Plans
    Search Plan By Title              ${title}   
    Register Should Not Be Found


Cenario: Busca planos pelo título      

    ${fixture}       Get JSON            plans-search.json
    ${plans}         Set Variable        ${fixture['plans']}

    ${word}          Set Variable        ${fixture['word']}
    ${total}         Set Variable        ${fixture['total']}

    Remove Plan                          ${word}

    FOR     ${item}         IN          @{plans}
        Insert Plan       ${item}
    END

    Go To Plans
    Search Plan By Title            ${word}  

    FOR     ${item}         IN          @{plans}
        Plan Should Be visible      ${item['title']}
    END

    Total Items Should Be               ${total}