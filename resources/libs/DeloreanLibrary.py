import psycopg2
from logging import info

class DeloreanLibrary():

    def connect(self):
        return psycopg2.connect(
            host='ec2-3-211-37-117.compute-1.amazonaws.com',
            database='dc84cbn2l8kmsd',
            user='mlypzmdpwivijp',
            password='1fc9a47d6517b5b3ced4540a8fe437c7371667e270c79f05c5aaf9364fa4cda5'
        )

    def remove_student(self, email):

        #No Robot vira uma KW automagicamente => Remove Student     email@desejado.com
        query = "delete from students where email = '{}'".format(email)
        info(query)

        conn = self.connect()

        cur = conn.cursor()
        cur.execute(query)
        conn.commit()
        conn.close()

    def remove_student_by_name(self, name):

        query = "delete from students where name LIKE '%{}%'".format(name)
        info(query)

        conn = self.connect()

        cur = conn.cursor()
        cur.execute(query)
        conn.commit()
        conn.close()


    def insert_student(self, student):

        self.remove_student(student['email'])

        query = ("insert into students (name,  email, age, weight, feet_tall, created_at, updated_at)"
                "values('{}', '{}', {}, {}, {}, now(), now());"
                .format(student['name'], student['email'], student['age'], student['weight'], student['feet_tall']))
        info(query)

        conn = self.connect()

        cur = conn.cursor()
        cur.execute(query)
        conn.commit()
        conn.close()


    def remove_plan(self, title):

        query = "delete from plans where title = '{}'".format(title)
        info(query)

        conn = self.connect()

        cur = conn.cursor()
        cur.execute(query)
        conn.commit()
        conn.close()


    def insert_plan(self, plan):

        self.remove_plan(plan['title'])

        query = ("insert into plans (title, duration, price, created_at, updated_at)"
                "values('{}', {}, {}, now(), now());"
                .format(plan['title'], plan['duration'], plan['price']))
        info(query)

        conn = self.connect()

        cur = conn.cursor()
        cur.execute(query)
        conn.commit()
        conn.close()